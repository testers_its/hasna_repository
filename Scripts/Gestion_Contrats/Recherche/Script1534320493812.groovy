import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testdata.ExcelData as ExcelData

ExcelData data = findTestData('Recherche')

for (def index : (1..data.getRowNumbers())) {
    WebUI.openBrowser('')

    WebUI.navigateToUrl(findTestData('Referentiel').getValue(2, 4))

    WebUI.maximizeWindow()

    WebUI.waitForElementVisible(findTestObject('Login/login_section'),10, FailureHandling.STOP_ON_FAILURE)

    WebUI.setText(findTestObject('Login/login_textbox'), findTestData('Recherche').getValue(1, index))

    WebUI.setText(findTestObject('Login/password_textbox'), findTestData('Recherche').getValue(2, index))

    WebUI.click(findTestObject('Login/login_button'))

    WebUI.waitForPageLoad(20, FailureHandling.STOP_ON_FAILURE)

    WebUI.navigateToUrl(findTestData('Referentiel').getValue(2, 6))

    WebUI.waitForPageLoad(20, FailureHandling.STOP_ON_FAILURE)

    WebUI.waitForElementClickable(findTestObject('Rechercher/Section_Recherche'), 10)

    WebUI.setText(findTestObject('Rechercher/Nom'), findTestData('Recherche').getValue(3, index))

    WebUI.click(findTestObject('Rechercher/Btn_Rech'))

    WebUI.waitForElementVisible(findTestObject('Rechercher/Resultat'),5)

    WebUI.click(findTestObject('Rechercher/Selection_btn'))

    WebUI.delay(findTestData('Referentiel').getValue(2, 1))
}

